import React, { Component } from 'react';
import { View, Text, ScrollView, FlatList, Modal, StyleSheet, TextInput, Alert, PanResponder, Share } from 'react-native';
import { Card, Icon, Rating, Button} from 'react-native-elements';
import{ DISHES } from '../shared/dishes';
import { COMMENTS } from '../shared/comments';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { postComment, postFavorite } from '../redux/ActionCreators';
import { comments } from '../shared/comments';
import * as Animatable from 'react-native-animatable';


const mapStateToProps = state => {
    return {
        dishes: state.dishes,
        comments: state.comments,
        favorites: state.favorites, 
    }
}

const mapDispatchToProps = dispatch => ({
    postFavorite: (dishId) => dispatch(postFavorite(dishId)),
    postComment: (dishId, rating, author, comment) => dispatch(postComment(dishId, rating, author, comment)) 
})



function RenderDish(props) {
    const dish = props.dish;

    handleViewRef = ref => this.view = ref;

    const recognizeDrag = ({ moveX, moveY, dx, dy}) => {
        if ( dx < -200 )
            return true;
        else 
            return false;
    };

    const recognizeComment = ({ moveX, moveY, dx, dy }) => {
        if ( dx > 200 )
            return true;
        else
            return false;
    }

    const panResponder = PanResponder.create({
        onStartShouldSetPanResponder: (e, gestureState) => {
            return true; 
        },
        onPanResponderGrant: () => {
            this.view.rubberBand(1000)
                .then(endState => console.log(endState.finished ? 'finished' : 'cancelled'));
        },

        onPanResponderEnd: (e, gestureState) => {
            if(recognizeDrag(gestureState)) {
                Alert.alert (
                    'Add to Favorite',
                    'Are you sure you wish to add ' + dish.name + ' to your favorites?',
                    [
                        {
                            text: 'Cancel',
                            onPress: () => console.log('Cancel pressed'),
                            style: 'cancel' 
                        },
                        {   
                            text: 'OK',
                            onPress: () => props.favorites ? console.log('Already favorite') : props.onPress()
                        }
                    ],
                    { cancelable: false }
                )
            }
            if(recognizeComment(gestureState)) {
                props.onCommentAdd();
            }
            return true; 

        }
    });

    const shareDish = (title, message, url) => {
        Share.share({
            title: title, 
            message: title + ': ' + message + ' ' + url,
            url: url 
        }, {
            dialogTitle: 'Share ' + title 
        });
    }


    if(dish != null){
        return(
            <Animatable.View animation="fadeInDown" duration={2000} delay={1000} 
                ref={this.handleViewRef}
                {...panResponder.panHandlers}>
                <Card
                    featuredTitle = {dish.name}
                    image={{ uri: baseUrl + dish.image }}
                    >
                    <Text style={{margin:10}}>
                        {dish.description}
                    </Text>
                    <View
                        style={{
                            paddingVertical: 15,
                            paddingHorizontal: 10,
                            flexDirection: "row",
                            alignItems: "center"
                        }}
                        >
                    <Icon
                        raised 
                        reverse 
                        name={ props.favorite ? 'heart' : 'heart-o' }
                        type='font-awesome'
                        color='#f50'
                        onPress={() => props.favorite ? console.log('Already favorite') : props.onPress()}
                    />
                    <Icon
                        reverse 
                        name= "pencil"
                        type='font-awesome'
                        color='rgb(81, 45, 168)'
                        onPress={() => props.onCommentAdd()}
                        />
                    <Icon 
                        raised 
                        reverse 
                        name={'share'}
                        type='font-awesome'
                        color='#51D2A8'
                        style={styles.cardItem}
                        onPress={() => shareDish(dish.name, dish.description, baseUrl + dish.image)}
                        />
                    </View>
                </Card>
            </Animatable.View>
        );
    }
    else {
        return(
            <View></View>
        );
    }
}

function RenderComments(props){
    const comments = props.comments;

    const renderCommentItem = ({ item, index }) => {
        return(
            <View key={index} style={{margin: 10}}>
                <Text style={{fontSize: 14}}>{item.comment}</Text>
                <Text style={{fontSize: 12}}>{item.rating} Stars</Text>
                <Text style={{fontSize: 12}}>{'--' + item.author + ',' + item.date}</Text>
            </View>
        );
    }
    return (
        <Animatable.View animation="fadeInUp" duration={2000} delay={1000}>
            <Card title="Comments">
                <FlatList
                    data={comments}
                    renderItem={renderCommentItem}
                    keyExtractor={item => item.id.toString()}
                />
            </Card>
        </Animatable.View>
    );
}

class Dishdetail extends Component {

    constructor(props){
        super(props)
        this.state = {
            rating: 1,
            author: '',
            comment: '',
            showModal: false
        }
    }

    toggleModal = () => {
        this.setState({showModal: !this.state.showModal});
    }

    handleComment = () => {
        console.log(this.state);
        const dishId = this.props.navigation.getParam('dishId', '');
        this.props.postComment(dishId, this.state.rating, this.state.author, this.state.comment);
        this.toggleModal();
        this.resetForm();
    };

    resetForm = () => {
        this.setState({
            rating: 1,
            author: '',
            comment: ''
        });
    };

    markFavorite(dishId) {
        this.props.postFavorite(dishId);
    };

    static navigationOptions = {
        title: 'Dish Details'
    };

    render(){
        const dishId = this.props.navigation.getParam('dishId', '')
        return(
            <>
                <ScrollView>
                    <RenderDish dish={this.props.dishes.dishes[dishId]} 
                        favorite={this.props.favorites.some(el => el === dishId)}
                        onPress={() => this.markFavorite(dishId)}
                        onCommentAdd={() => this.toggleModal()}
                    />
                    <RenderComments comments={this.props.comments.comments.filter((comment) => comment.dishId === dishId)} />
                </ScrollView>
                <Modal animationType = {'slide'} transparent = {false}
                    visible = {this.state.showModal}
                    onRequestClose = {() => this.toggleModal() }
                    >
                    <View style = {styles.modal}>
                        <Rating
                            showRating
                            style={{ paddingVertical: 10 }}
                            itemValue={this.state.rating}
                            onFinishRating={(value) => this.setState({rating: value})}
                        />
                        <View style={styles.formRow}>
                            <Icon name="user" type='font-awesome' size={20} color="#000"/>
                            <TextInput
                                style={styles.textInput}
                                placeholder="Author"
                                maxLength={20}
                                onChangeText={(itemValue) => this.setState({author: itemValue})}
                            />
                        </View>
                        <View style={styles.formRow}>
                            <Icon name='comment' type='font-awesome' size={20} color='grey' />
                            <TextInput
                                    style={styles.textInput}
                                    placeholder="Comment"
                                    maxLength={20}
                                    onChangeText={(value) => this.setState({comment: value})}
                                />
                        </View>
                        <Button buttonStyle={styles.submitButton}
                            onPress = {() => this.handleComment()}
                            title='Submit'
                            color='blue'
                            />
                        <Button buttonStyle={styles.cancelButton}
                            onPress = {() => this.toggleModal()}
                            title='Cancel'
                            color='darkgrey'
                            />
                    </View>
                </Modal>
            </>
        );
    }
};

const styles = StyleSheet.create({
    formRow: {
        flexDirection: 'row',
        margin: 20,
    },
    modal: {
        justifyContent: 'center',
        margin: 20
    },
    textInput: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 0,
        backgroundColor: '#fff',
        color: '#424242',
    },
    submitButton: {
        backgroundColor: '#512DA8'
    },
    cancelButton: {
        backgroundColor: '#808080'
    }
})



export default connect(mapStateToProps, mapDispatchToProps)(Dishdetail);