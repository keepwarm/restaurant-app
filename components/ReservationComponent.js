import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet, Picker, Switch, Button, Modal, Alert, PanResponder, Platform } from 'react-native';
import { Card } from 'react-native-elements';
//import DatePicker from 'react-native-datepicker';
import * as Animatable from 'react-native-animatable';
//import { Permissions, Notifications } from 'expo';
//import { Permissions, Notifications } from 'react-native-permissions';
import * as Permissions from 'expo-permissions';
import * as Notifications from 'expo-notifications';
import DateTimePicker from '@react-native-community/datetimepicker';
import * as Calendar from 'expo-calendar';

class Reservation extends Component {

    constructor(props){
        super(props);
        this.state={
            guests: 1,
            smoking: false,
            date: new Date(),
            showDatePicker: false
            //showModal: false 
        }
    }
    static navigationOptions ={
        title: 'Reverse Table'
    }

    // toggleModal(){
    //     this.setState({ showModal: !this.state.showModal })
    // }

    showAlert() {
        Alert.alert (
            'Your Reservation OK?',
            `Number of Guests: ${this.state.guests}\nSmoking? ${this.state.smoking}\nDate and Time: ${this.state.date}`,
            [
                {
                    text: 'Cancel',
                    onPress: () => this.resetForm(),
                    style: 'cancel' 
                },
                {   
                    text: 'OK',
                    onPress: () => {
                        this.presentLocalNotification(this.state.date);
                        this.resetForm();
                    }
                }
            ],
            { cancelable: false }
        )
    }

    async handleReservation(){
        //console.log(JSON.stringify(this.state));
        //this.toggleModal();
        this.showAlert();
        const status = await this.obtainCalendarPermission();
        if (status === 'granted') {
            this.addReservationToCalendar(this.state.date);
        }
    }

    resetForm(){
        this.setState({
            guests: 1,
            smoking: false,
            date: new Date(),
            showDatePicker: false
        })
    }

    async obtainNotificationPermission(){
        console.log('inside obtainNotificationPermission()');
        let permission = await Permissions.getAsync(Permissions.USER_FACING_NOTIFICATIONS);
        if ( permission.status !== 'granted'){
            permission = await Permissions.askAsync(Permissions.USER_FACING_NOTIFICATIONS);
            if (permission.status !== 'granted'){
                Alert.alert('Permission not granted to show notifications');
            }
        }
        return permission;
    }

    async obtainCalendarPermission(){
        const {status} = await Calendar.requestCalendarPermissionsAsync();
        return status;
    }

    async addReservationToCalendar(date){
        const calendars = await Calendar.getCalendarsAsync();
        // if there are any calendars
        if (calendars.length) {
            // get the first calendar
            
            const defaultCalendar = calendars[0];
            const startDate = new Date(Date.parse(date));
            console.log(startDate);
            const endDate = new Date(startDate.getTime() + (2*60*60*1000));
            Calendar.createEventAsync(defaultCalendar.id, {
                title: 'Con Fusion Table Reservation',
                startDate,
                endDate,
                timeZone: 'Asia/Hong_Kong',
                location: '121, Clear Water Bay Road, Clear Water Bay, Kowloon, Hong Kong'
            });
        }
    }

    async presentLocalNotification(date){
        console.log('inside presentLocalNotification()');
        await this.obtainNotificationPermission();
        // Notifications.presentLocalNotificationAsync({
        //     title: 'Your Reservation',
        //     body: 'Reservation for ' + date + ' requested',
        //     ios: {
        //         sound: true
        //     },
        //     android: {
        //         sound: true,
        //         vibrate: true,
        //         color: '#512DA8'
        //     }
        // });
        console.log('if platform...');
        console.log(Platform.OS);
        if (Platform.OS === 'android') {
            const channel = await Notifications.setNotificationChannelAsync('default', {
                name: 'default',
                importance: Notifications.AndroidImportance.MAX
            });
            //const channels = Notifications.getNotificationChannelAsync()
            console.log('CHANNEL ID');
            console.log(channel.id);
            const notification = await Notifications.scheduleNotificationAsync({
                channelId: channel.id,
                content: {
                    title: 'Your Reservation'
                },
                trigger: {
                    seconds: 0,
                    repeats: false
                }
            });
            console.log(notification);
        }
    }

    onDateChange(event, date) {
        this.setState({date: date, showDatePicker: false});
    }

    render(){
        return(
            <ScrollView>
                <Animatable.View animation="zoomIn" duration={2000} delay={1000}>
                    <View style={styles.formRow}>
                        <Text style={styles.formLabel}> Number of Guest</Text>
                        <Picker
                            style={styles.formItem}
                            selectedValue={this.state.guests}
                            onValueChange={(itemValue, itemIndex) => this.setState({guests: itemValue})}
                            >
                            <Picker.Item label='1' value='1' />
                            <Picker.Item label='2' value='2' />
                            <Picker.Item label='3' value='3' />
                            <Picker.Item label='4' value='4' />
                            <Picker.Item label='5' value='5' />
                            <Picker.Item label='6' value='6' />
                        </Picker>
                    </View>
                    <View style={styles.formRow}>
                        <Text style={styles.formLabel}> Smoking/Non-smoking?</Text>
                        <Switch
                            style={styles.formItem}
                            value={this.state.smoking}
                            onTintColor='#512DAB'
                            onValueChange={(value) => this.setState({ smoking:value })}    
                        >
                        </Switch>
                    </View>
                    <View style={styles.formRow}>
                    <Text style={styles.formLabel}>Date and Time</Text>
                    <Button 
                        style={styles.formItem}
                        title="Select Date"
                        onPress={() => this.setState({showDatePicker: true})} />
                    {this.state.showDatePicker && (
                        <DateTimePicker
                            value={this.state.date}
                            mode="datetime"
                            display="default"
                            onChange={this.onDateChange}
                        />
                    )}
                    {/* <DatePicker
                        style = {{ flex: 2, marginRight: 20}}
                        date={this.state.date}
                        format=''
                        mode='datetime'
                        placeholder='select date and time'
                        minDate='2017-01-01'
                        confirmBtnText='Confirm'
                        cancelBtnText='Cancel'
                        customStyles={{
                            dateIcon: {
                                position: 'absolute',
                                left: 0,
                                top: 4,
                                marginLeft: 0
                            },
                            dateInput: {
                                marginLeft: 36
                            }
                        }}
                        onDateChange={(date) => {this.setState({ date: date })}}
                    /> */}
                    </View>
                    <View style={styles.formRow}>
                        <Button 
                            buttonStyle={styles.submitButton}
                            title="Reserve"
                            onPress={() => this.handleReservation()}
                            accessibilityLabel="Learn more about this purple button"
                        />
                    </View>
                    {/* <Modal
                        animationType={'slide'}
                        transparent={false}
                        visible={this.state.showModal}
                        onDismiss={() => {this.toggleModal(); this.resetForm()}}
                        onRequestClose={() => {this.toggleModal(); this.resetForm()}}
                    >
                        <View style={styles.modal}>
                            <Text style={styles.modalTitle}>Your Reservation</Text>
                            <Text style={styles.modalText}>Number of Guests: {this.state.guests}</Text>
                            <Text style={styles.modalText}>Smoking? : {this.state.smoking ? 'Yes' : 'No'}</Text>
                            <Text style={styles.modalText}>Date and Time: {this.state.date}</Text>
                            <Button
                                onPress={() => {this.toggleModal(); this.resetForm()}}
                                color="#512DA8"
                                title="Close"
                            />
                        </View>
                    </Modal> */}
                </Animatable.View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    formRow: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'row',
        margin: 20
    },
    formLabel: {
        fontSize: 18,
        flex: 2
    },
    formItem: {
        flex: 1
    },
    modal: {
        justifyContent: 'center',
        margin: 20
    },
    modalTitle: {
        fontSize: 24, 
        fontWeight: 'bold',
        backgroundColor: '#512DA8',
        textAlign: 'center',
        color: 'white',
        marginBottom: 20
    },
    modalText: {
        fontSize: 18,
        margin: 10
    },
    submitButton: {
        backgroundColor: '#512DA8'
    },
})

export default Reservation;
